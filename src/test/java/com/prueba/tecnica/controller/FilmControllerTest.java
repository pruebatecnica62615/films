package com.prueba.tecnica.controller;

import com.prueba.tecnica.dto.Response;
import com.prueba.tecnica.service.FilmService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class FilmControllerTest {

    @InjectMocks
    private FilmController filmController;

    @Mock
    private FilmService filmService;

    @BeforeEach

    public void setUp () {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void createFilmTest()  {
        when(filmService.findFilm(any())).thenReturn(new Response(1,"prueba","prueba2",200, "ok"));
        Response response = filmController.createFilm("1");
        assertEquals(200, response.getCode());
        assertEquals("ok", response.getDescription());
    }

    @Test
    void findFilmTest()  {
        when(filmService.findById(any())).thenReturn(new Response(1,"prueba","prueba2",200, "ok"));
        Response response = filmController.findFilm(1);
        assertEquals(200, response.getCode());
        assertEquals("ok", response.getDescription());
    }

    @Test
    void deleteFilmTest()  {
        when(filmService.deleteFilms(any())).thenReturn(new Response(1,"prueba","prueba2",200, "ok"));
        Response response = filmController.deleteFilm(1);
        assertEquals(200, response.getCode());
        assertEquals("ok", response.getDescription());
    }

    @Test
    void updateFilmTest()  {
        when(filmService.updateFilm(any(),any())).thenReturn(new Response(1,"prueba","prueba2",200, "ok"));
        Response response = filmController.updateFilm(1,null);
        assertEquals(200, response.getCode());
        assertEquals("ok", response.getDescription());
    }
}
