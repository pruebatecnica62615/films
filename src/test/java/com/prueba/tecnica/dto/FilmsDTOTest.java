package com.prueba.tecnica.dto;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

class FilmsDTOTest {

    @InjectMocks
    private FilmsDTO filmsDTO = new FilmsDTO();

    @Test
    void validateEpisode_id()
    {
        filmsDTO.setEpisode_id(1);
        Assert.assertEquals("1", filmsDTO.getEpisode_id().toString());
    }

    @Test
    void validateTitle()
    {
        filmsDTO.setTitle("prueba");
        Assert.assertEquals("prueba", filmsDTO.getTitle());
    }

    @Test
    void validateRelease_date()
    {
        filmsDTO.setRelease_date("prueba2");
        Assert.assertEquals("prueba2", filmsDTO.getRelease_date());
    }

    @Test
    void validateBuilder()
    {
        FilmsDTO build = FilmsDTO.builder()
                .title("prueba")
                .episode_id(1)
                .release_date("prueba2")
                .build();
        Assert.assertNotNull(build.toString());
    }
}
