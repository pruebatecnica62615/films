package com.prueba.tecnica.service;

import com.prueba.tecnica.dto.Response;
import com.prueba.tecnica.entities.Films;
import com.prueba.tecnica.repository.FilmsRepository;
import com.prueba.tecnica.service.imp.FilmServiceImp;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.RestTemplate;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
class FilmServiceTest {

    @Mock
    private FilmsRepository filmsRepository;

    @InjectMocks
    private FilmServiceImp filmServiceImp;

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private Environment env;

    private Response response = new Response();

    private Films films = new Films();

    public void setUp () {
        MockitoAnnotations.initMocks(this);

        response.setRelease_date("prueba");
        response.setTitle("prueba2");
        response.setEpisode_id(1);
        response.setCode(200);
        response.setDescription("Exitoso");

        films.setRelease_date("prueba2");
        films.setEpisodeId(1);
        films.setTitle("prueba");
        films.setId(1);
    }

    @Test
    public void createFilmsTest() {

        Response response = new Response();
        response.setEpisode_id(1);
        response.setTitle("Title");
        response.setRelease_date("2022-01-01");

        when(filmsRepository.save(any())).thenAnswer(invocation -> invocation.getArgument(0));

        Films result = filmServiceImp.createFilms(response);

        verify(filmsRepository, times(1)).save(any(Films.class));
        assertEquals(response.getEpisode_id(), result.getEpisodeId());
        assertEquals(response.getTitle(), result.getTitle());
        assertEquals(response.getRelease_date(), result.getRelease_date());
    }

    @Test
    void findByIdTest () {
        when(filmsRepository.findByEpisodeId(any())).thenReturn(films);
        Response response = filmServiceImp.findById(1);
        assertEquals(null, response.getTitle());
    }


    @Test
    void deleteFilmsTest () {
        when(filmsRepository.findByEpisodeId(any())).thenReturn(films);
        Response response = filmServiceImp.deleteFilms(1);
        assertEquals(null, response.getTitle());
    }


    @Test
    void updateFilmTest () {
        when(filmsRepository.findByEpisodeId(any())).thenReturn(films);
        Response response = filmServiceImp.updateFilm(null,1);
        assertEquals(null, response.getTitle());
    }
    @Test
    void findFilmTest() {
        Response myEntity = new Response();
        when(restTemplate.exchange(anyString(), any(HttpMethod.class), any(),
                ArgumentMatchers.<Class<String>>any())).thenReturn(null);
        when(env.getProperty(any())).thenReturn("http://test");
        Response response = filmServiceImp.findFilm("1");
        assertNull(null, response.getTitle());
    }
}
