package com.prueba.tecnica.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Component
public class Response {

	@JsonProperty("episode_id")
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Integer episode_id;
	@JsonProperty("title")
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String title;

	@JsonProperty("release_date")
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String release_date;

	@JsonProperty("code")
	private Integer code;

	@JsonProperty("description")
	private String description;

	@Override
	public String toString() {
		return "{\"episode_id\":\"" + episode_id + "\", \"title\":\"" + title + "\", \"code\":\"" + code + "\", \"description\":\"" + description + "\"}";
	}

}