package com.prueba.tecnica.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class FilmsDTO {

	private Integer episode_id;

	private String title;

	private String release_date;

	public FilmsDTO(){

	}
}
