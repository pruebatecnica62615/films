package com.prueba.tecnica.service.imp;

import com.prueba.tecnica.dto.FilmsDTO;
import com.prueba.tecnica.dto.Response;
import com.prueba.tecnica.util.Constants;
import com.prueba.tecnica.entities.Films;
import com.prueba.tecnica.repository.FilmsRepository;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import com.prueba.tecnica.service.FilmService;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Service
@Log
public class FilmServiceImp implements FilmService {

	@Autowired
	private Environment env;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	FilmsRepository filmsRepository;

	@Override
	public Response findFilm (String id) throws HttpClientErrorException {
		try {
			int idLength = id.length();
			if(idLength >= 2){
				log.info("El parametro es demasiado grande");
				Response response = new Response();
				response.setCode(400);
				response.setDescription("Error en la solicitud");
				return response;
			}

			if (contieneLetras(id)) {
				log.info("El parámetro contiene al menos una letra.");
				Response response = new Response();
				response.setCode(400);
				response.setDescription("Error en la solicitud");
				return response;
			}
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<Void> entity = new HttpEntity<>(headers);
			String url = getUrl().replace("@Id", id);
			log.info("url de consumo: " + url);
			Response response = restTemplate.exchange(url, HttpMethod.GET, entity, Response.class).getBody();
			log.info("response: " + restTemplate.exchange(url, HttpMethod.GET, entity, String.class).getBody());
			if (response.getEpisode_id() != null || !response.equals(null)) {
				this.createFilms(response);
				response.setCode(200);
				response.setDescription("Proceso Exitoso");
			}
			return response;

		}catch (Exception e){
			Response response = new Response();
			response.setCode(204);
			response.setDescription("Sin Resultados");
			return response;
		}
	}

	private String getUrl() {
		String endpoint = env.getProperty(Constants.PATH_URL);
		return endpoint;
	}

	public static boolean contieneLetras(String str) {
		return str.matches(".*[a-zA-Z].*");
	}

	public Films createFilms (Response response) {
		Films films = new Films();
		films.setEpisodeId(response.getEpisode_id());
		films.setTitle(response.getTitle());
		films.setRelease_date(response.getRelease_date());
		filmsRepository.save(films);
		log.info("Registro Guardado con Exito: " + films.getTitle());
		return films;
	}
	@Override
	public Response findById (Integer episode_id){
		try {
			Response response = new Response();
			Films films = filmsRepository.findByEpisodeId(episode_id);
			FilmsDTO filmDto = FilmsDTO.builder()
					.episode_id(films.getEpisodeId())
					.release_date(films.getRelease_date())
					.title(films.getTitle())
					.build();
			if (filmDto.getEpisode_id() != null || !filmDto.equals(null)) {
				response.setTitle(filmDto.getTitle());
				response.setEpisode_id(filmDto.getEpisode_id());
				response.setRelease_date(filmDto.getRelease_date());
				response.setCode(200);
				response.setDescription("Proceso Exitoso");
			}
			return response;

		}catch (Exception e){
			Response response = new Response();
			response.setCode(204);
			response.setDescription("Sin Resultados");
			return response;
		}
	}
	@Override
	public Response deleteFilms (Integer episode_id) {

		try {
			Response response = new Response();
			Films films = filmsRepository.findByEpisodeId(episode_id);
			filmsRepository.delete(films);
			response.setCode(200);
			response.setDescription("Borrado Exitoso");
			return response;

		} catch (Exception e) {
			Response response = new Response();
			response.setCode(204);
			response.setDescription("Sin Resultados para eliminar");
			return response;
		}
	}
	@Override
	public Response updateFilm (FilmsDTO filmsDTO, Integer episode_id){
		try {
			Response response = new Response();
			Films films = filmsRepository.findByEpisodeId(episode_id);
			films.setEpisodeId(filmsDTO.getEpisode_id());
			films.setTitle(filmsDTO.getTitle());
			films.setRelease_date(filmsDTO.getRelease_date());
			filmsRepository.save(films);
			response.setCode(200);
			response.setDescription("Actualizacion Exitosa");
			return response;
		}catch (Exception e) {
			Response response = new Response();
			response.setCode(204);
			response.setDescription("No se pudo actualizar");
			return response;
		}
	}
}
