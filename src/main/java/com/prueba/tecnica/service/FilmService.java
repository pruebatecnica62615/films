package com.prueba.tecnica.service;

import com.prueba.tecnica.dto.FilmsDTO;
import org.springframework.stereotype.Service;

import com.prueba.tecnica.dto.Response;

@Service
public interface FilmService {

	Response findFilm (String id);

	Response findById (Integer episode_id);

	Response deleteFilms (Integer episode_id);

	Response updateFilm (FilmsDTO FilmsDTO, Integer episode_id);

}
