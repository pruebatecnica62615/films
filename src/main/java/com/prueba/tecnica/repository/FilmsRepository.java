package com.prueba.tecnica.repository;

import com.prueba.tecnica.entities.Films;
import org.springframework.data.repository.CrudRepository;
import com.prueba.tecnica.entities.FilmsPK;

public interface FilmsRepository extends CrudRepository<Films, FilmsPK>{

    Films findByEpisodeId (Integer episode_id);
}
