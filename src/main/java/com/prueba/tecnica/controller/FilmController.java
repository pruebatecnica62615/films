package com.prueba.tecnica.controller;

import com.prueba.tecnica.dto.FilmsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import com.prueba.tecnica.dto.Response;
import com.prueba.tecnica.service.FilmService;

@EnableAutoConfiguration
@CrossOrigin(origins = "*")
@RequestMapping(path = "${controller.properties.base-path}")
@RestController
@Controller
public class FilmController {

	@Autowired
	private FilmService filmService;
	
	@GetMapping(value = "/film/{id}")
	public Response createFilm (@PathVariable(name="id", required = false, value = "") String id) {
	return filmService.findFilm(id);
	}

	@GetMapping(value = "/findFilm/{episode_id}")
	public Response findFilm (@PathVariable(name="episode_id", required = false, value = "") Integer episode_id) {
		return filmService.findById(episode_id);
	}

	@PostMapping(value = "/deleteFilm/{episode_id}")
	public Response deleteFilm (@PathVariable(name="episode_id", required = false, value = "") Integer episode_id) {
		return filmService.deleteFilms(episode_id);
	}

	@PostMapping(value = "/updateFilm/{episode_id}")
	public Response updateFilm (@PathVariable(name="episode_id", required = false, value = "") Integer episode_id,
								@RequestBody(required = true) FilmsDTO filmsDTO) {
		return filmService.updateFilm(filmsDTO,episode_id);
	}
}
