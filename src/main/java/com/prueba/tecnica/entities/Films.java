package com.prueba.tecnica.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "Films")
@IdClass(FilmsPK.class)
@Setter
@Getter
@Data
public class Films implements Serializable{
	
	private static final long serialVersionUID = -9005522915247534874L;

	@Id
	@Column(nullable = false, name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(nullable = false, name = "episode_id")
	private Integer episodeId;

	@Column(nullable = false, name = "title")
	private String title;

	@Column(nullable = false, name = "release_date")
	private String release_date;

}
