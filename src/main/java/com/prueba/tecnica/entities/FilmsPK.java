package com.prueba.tecnica.entities;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Id;
@Getter
@Setter
public class FilmsPK implements Serializable{
	
	private static final long serialVersionUID = -9005522915247534874L;
	
	@Id
	@Column(nullable = false, name = "id")
	private long id;
	
	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		FilmsPK that = (FilmsPK) o;
		return id == that.id;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

}
